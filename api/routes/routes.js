"use strict";

module.exports = function(app) {
  // var todoList = require("../controllers/controller");
  var scrape = require("../../scripts/scraper");
  var managers = require("../../scripts/managers2");
  var auction = require("../../scripts/auction");
  var ratings = require("../../scripts/ratings");
  var players = require("../../scripts/players");
  var teams = require("../../scripts/teams");

  // todoList Routes
  // app
  //   .route("/tasks")
  //   .get(todoList.list_all_tasks)
  //   .post(todoList.create_a_task);
  //   // .post(managers.add_manager);

  // app
  //   .route("/tasks/:taskId")
  //   .get(todoList.read_a_task)
  //   .put(todoList.update_a_task)
  //   .delete(todoList.delete_a_task);

  app.route("/scrape").get(scrape.updatePlayers); //  need to be run at the start of every season
  app.route("/scrape_teams").get(scrape.updateTeams); //  need to be run at the start of every season

  app
    .route("/managers")
    .get(managers.managers_all) //  working! PULLS ALL MANAGERS
    .post(managers.managers_add) //  working! ADDS A MANAGER
    .delete(managers.manager_delete); //  working!  DELETES ONE MANAGER

  app
    .route("/manager/:name")
    .get(managers.manager_get) //  working! Pulls all players and auction events for that manager
    .put(managers.manager_put); //  is this needed? I don't think so...

  app
    .route("/auction")
    .get(auction.events_all) //  working!
    .post(auction.event_add) //  working! takes time,player,manager,price
    .delete(auction.event_delete2); //  working! takes player,manager,price

  app
    .route("/ratings")
    .get(ratings.ratings_all) //  working!
    .post(ratings.rating_add2) //  working!
    .delete(ratings.rating_delete2); //  working!

  app.route("/players")
    .get(players.players_get);

  app.route("/players/:teamAbbrev")
    .get(players.players_get_team);

  app.route("/player/:playerName")
    .get(players.player_get);

  app.route("/teams")
    .get(teams.teams_all);
};

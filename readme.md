# FF v 4.0 (2018)

Auction tool for fantasyleague.com auctions.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Access to this repo for starters

### Installing

Just like, install it. I dunno. Local or...

## Deployment

Add additional notes about how to deploy this on a live system

# API Documentation

Here's what you're after, the data, how it's pulled and what it returns. w00t.

---

## Players
Deals with adding and updating of basic player data into the players collection

---

### GET /scrape

Simple URL, pulls all of the data from [Fantasy League](http://www.fantasyleague.com) and inserts it into the mongodb. If the player already exists in there, they are left, if they have changed club or position, that is changed.

---

### GET /players

* accepts nothing (yet) returns all players in the form of:
```
{
    "_id": "5b716655dcee975047e2c8cb",
    "playerName": "S Duffy",
    "position": "CB",
    "club": "BHA",
    "statPld": "1",
    "statGls": "0",
    "statAss": "0",
    "statCs": "0",
    "statGa": "2",
    "statPts": "-1",
    "updateTime": "2018-08-13T12:06:59+01:00"
}
```
Should all be pretty self-explanatory. updateTime is in [Momentjs](https://momentjs.com/) format.

---

###   GET /players/:teamName
* Takes a team name and returns a subset of all players in that team. Add the team name after the slash.

---

###   GET /player/:playerName
* Takes a player name and returns all of their details and ratings in the form of:

```
{
    "details": 
        {
            "_id": "5b716655dcee975047e2caa7",
            "playerName": "H Kane",
            "position": "ST",
            "club": "TOT",
            "statPld": "1",
            "statGls": "0",
            "statAss": "0",
            "statCs": "0",
            "statGa": "0",
            "statPts": "0",
            "updateTime": "2018-08-13T12:07:00+01:00"
        },
    "ratings": 
        {
            "_id": "5b71df7b01d5126431d67ae7",
            "playerName": "H Kane",
            "position": "ST",
            "club": "TOT",
            "notes": "Great player, best S and well worth the money, however much that is...",
            "rating": 10,
            "definite": true,
            "estimate": 15
        }
}
```

---

## Managers
Deals with additions and deletions from the managers collection

---

### GET /managers
* accepts nothing, returns all managers in the form of:

```
{
    "response": "all managers sent",
    "data": [
        {
            "_id": "5b71928b0e58455a42d2890d",
            "managerName": "Richard"
        },
        {
            "_id": "5b7195891443595addb67fe8",
            "managerName": "Greg"
        }
    ]
}
```

---

### POST /managers
* accepts simple object and adds it to the managers collection in the form of:
```
{"managerName": "Richard"}
```

* returns all managers in the same form as the GET call

---

### DELETE /managers
* accepts simple object and removes it form the managers collection in the form of:
```
{"managerName": "Richard"}
```

* returns all managers in the same form as the GET call

---

###   GET /manager/:managerName
* Takes a manager name and returns all players they have bought by position. Add the manager name after the slash to return just that manager in the form of:

```
{
    "response": "1",
    "data": {
        "GK": [],
        "FB": [
            {
                "playerName": "D Rose",
                "price": 4,
                "position": "FB",
                "club": "TOT"
            }
        ],
        "CB": [],
        "MF": [
            {
                "playerName": "A Martial",
                "price": 2,
                "position": "MF",
                "club": "MU"
            }
        ],
        "ST": []
    }
}
```
* response is either 0 (manager has bought no players) or 1 (manager has bought at least one player).

---

## Auctions
Deals with additions and deletions to the auction collection. This collects every auction event as the auction progresses

---

### GET /auction
* accepts nothing, returns all auction events in the form of:

```
[
    {
        "_id": "5b71b360f81d2e5f7c457964",
        "time": "10:04pm",
        "player": "J Vertonghen",
        "club": "TOT",
        "posotion": "CB",
        "manager": "Richard",
        "price": 5.5
    },
    {
        "_id": "5b71bbba7135d861f64d323a",
        "time": "10:09pm",
        "player": "J Vardy",
        "club": "LEI",
        "position": "ST",
        "manager": "Gavin",
        "price": 10.5
    }
]
```
* time will be in [Momentjs](http://www.momentjs.com) format
* not sure an array quite makes sense. Or it does. Other calls return an object with an array inside. Maybe I should standardise this.

---

### POST /auction
* accepts an auction event and adds it to the auction collect in the form of:
```
{
    "time": [momentjs],
    "player": [playerName],
    "club": [club],
    "position": [position],
    "manager": managerName,
    "price": [Number]
}
```
* returns all auction events in the form of GET /auction

---

### DELETE /auction
* accepts an auction id and deletes it from the auction collect in the form of:
```
{"id": "5b71b89fd02433604f238eb2"}
```
* returns all auction events in the form of GET /auction

---

## Ratings
Deals with additions and deletions to the ratings collection. This collects every rating event as they are added.

---

### GET /ratings
* accepts nothing, returns all auction events in the form of:
```
[
    {
        "_id": "5b71df7b01d5126431d67ae7",
        "playerName": "H Kane",
        "position": "S",
        "club": "TOT",
        "notes": "Great player, best S and well worth the money, however much that is...",
        "rating": 10,
        "definite": true,
        "estimate": 15
    },
    {
        "_id": "5b71df9f01d5126431d67ae8",
        "playerName": "T Heaton",
        "position": "G",
        "club": "BUR",
        "notes": "Great player, best S and well worth the money, however much that is...",
        "rating": 7,
        "definite": false,
        "estimate": 2
    }
]
```
---

### POST /ratings
* accepts a rating event and adds it to the ratings collection in the form of:
```
{
    "playerName": "T Heaton",
    "position": "G",
    "club": "BUR",
    "notes": "Blah blah blah",
    "rating": 6,
    "reserve": false,
    "estimate": 2
}
```
* returns a list of all rating events - NOT ANY MORE
* actually updates document based on a search for playerName/club/position. If found, document is updated, if not it is added (upsert)

---

### DELETE /ratings
* accepts a rating id and deletes it from the ratings collection in the form of:
```
{"id": "5b72a41c901c9069fdcccdbf"}
```
* returns a list of all rating events

---

## Teams
Deals with additions and deletions of teams and their abbreviations

---

### GET /teams
* accepts nothing, returns all auction events in the form of:

```
[
    {
        "_id": "5b71b360f81d2e5f7c457964",
        "teamName": "Tottenham",
        "teamAbbreviation": "TOT",
        "isActive": true
    },
    {
        "_id": "5b71b360f81d2e5f7c457965",
        "teamName": "",
        "teamAbbreviation": "MC",
        "isActive": true
    }
]
```
* the listings the unique team abbreviations in the player list

---

## Known issues
* Players with the same name and same statPts aren't being properly differentiated and have their club changed when /scrape is run
* Not all objects returned are consistent. E.g. GET players, GET ratings and GET auctions are arrays whereas GET mangers is an object with the array inside. Should standardise.
* No access limitation - consider using [Okta](https://developer.okta.com/)

## Future functions
* Backup button - will take all data, store it elsewhere and clear the collections ready for a new season
* Mid-auction analysis - provide real-time stats and feedback as the auction progresses
* Simulation agents - run auctions with agents, store and analyse the data to prove/disprove emergent auction behaviour hypothesis
* Match real actioneers to emergent auction profiles mid-action to predict behaviour

## Built With

* [Node](https://nodejs.org/en/) - Because I'm not confident enough with GoLang
* [Express](https://expressjs.com/) - Because Node
* [Mongo](https://www.mongodb.com/) - Because it's easy

## Contributing

If you like. You'll need access here first though.

## Versioning

I know little about versioning.

I suppose I could use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **[Jonathan Shrago](http://www.twitter.com/jonathanshrago)**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* www.fantasyleague.com

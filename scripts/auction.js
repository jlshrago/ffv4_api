"use strict";

const settings = require("./settings");
const db = require("monk")(settings.mongoURL);
var activeCollection = settings.auctionCollection;


exports.events_all = function(req, res) {
  var collection = db.get(activeCollection);

  collection.find({}, function(err, docs) {
    res.send(docs);
  });
};

exports.event_add = function(req, res) {
  // this takes an event object, adds it to the auction collection and returns the whole collection - returning all of it because the client will need it during the auction anyway.
  var collection = db.get(activeCollection);

  collection.insert(req.body, function(err, docs) {
    // once inserted, pull all of the auction events and return them
    collection.find({}, function(err, docs) {
      // console.log(docs)
      res.send(docs);
    });
  });
};

exports.event_delete = function(req, res) {
  // receives the same auction object of time,player,manager,price and is matched on player, manager, price likely pulled from the object in the dom
  var collection = db.get(activeCollection);

  console.log(req.body.player);

  collection
    .findOneAndDelete({
      player: req.body.player,
      manager: req.body.manager,
      price: req.body.price
    })
    .then(doc => {
        // return all, updated auction events
        collection.find({}, function(err, docs) {

            res.send({ docs });
        })
    });
};

exports.event_delete2 = function(req, res) {
    // receives _id and deletes the update - makes more sense than the same object?
    var collection = db.get(activeCollection);
  
    console.log(req.body.player);
  
    collection
      .findOneAndDelete({
        _id: req.body.id
      })
      .then(doc => {
          // return all, updated auction events
          collection.find({}, function(err, docs) {
  
              res.send({ docs });
          })
      });
  };
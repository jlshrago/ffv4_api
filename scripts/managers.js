"use strict";

const settings = require("./settings");
const db = require("monk")(settings.mongoURL);
var activeCollection = settings.managerCollection;
var auctionCollection = settings.auctionCollection;
var playerCollection = settings.playerCollection;

// db.then(() => {
//     console.log("db connected in managers.js as well")
// })

exports.managers_all = function(req, res) {
  var collection = db.get(activeCollection);
  collection.find({}).then(docs => {
    //   console.log(docs)
    res.send({
      response: "all managers sent",
      data: docs
    });
  });
  console.log("Get all managers");
};

exports.managers_addOLD = function(req, res) {
  // add the manager to the database if he/she is not already in there
  var collection = db.get(activeCollection);
  collection.count({ managerName: req.body.managerName }, function(err, count) {
    if (count == 0) {
      // manager is not in managers collection, add them
      collection.insert(req.body);
      console.log("Manager added: ", req.body.managerName);

        // return all managers
        collection.find({}).then(docs => {
          //   console.log(docs)
          res.send({
            response: "all managers sent",
            data: docs
          });
        });

    } else {
      // manager already in db collection, ignore
      console.log("Manager " + req.body.managerName + " already in db");
      res.send({ response: "ignored" });
    }
  });
};

exports.managers_add = function(req, res) {
  // add the manager to the database if he/she is not already in there
  var collection = db.get(activeCollection);
  collection.count({ managerName: req.body.managerName }, function(err, count) {
    if (count == 0) {
      // manager is not in managers collection, add them
      collection.insert(req.body).then(result => {
        console.log("Manager added: ", req.body.managerName);
  
          // return all managers
          collection.find({}).then(docs => {
            //   console.log(docs)
            res.send({
              response: "all managers sent",
              data: docs
            });
          });

      });

    } else {
      // manager already in db collection, ignore
      console.log("Manager " + req.body.managerName + " already in db");
      res.send({ response: "ignored" });
    }
  });
};

exports.manager_get = function(req, res) {
  console.log(JSON.stringify(req.params.name));
  // retrieve all of the auction events relating to the manager AND all of the players from all of those auction events
  var collection1 = db.get(auctionCollection);
  var collection2 = db.get(playerCollection);

  collection1.find({manager: req.params.name }).then(docs => {
    var thePlayers = []
    // get the list of the players the manager has bought

    if(docs.length == 0) {
      res.send({
        response: "0"
      })
    }

    for(let i = 0; i < docs.length; i++) {
      var playerObj = {}
      playerObj["playerName"] = docs[i].player;
      thePlayers.push(playerObj)
    }
    
    // get data on the players
    collection2.find({ $or: thePlayers }).then(docs2 => {
      console.log(docs2)

      res.send({
        response: "1",
        auctionEvents: docs,
        playersBought: docs2
      })

    })
    
  })

}





//   collection.find({ managerName: req.params.name }).then(docs => {
//     // console.log(docs);
//     if (docs.length == 0) {
//       res.send({ response: "manager not in db" });
//     } else {
//       res.send({
//         response: "manager returned",
//         data: docs
//       });
//     }
//   });
// };

exports.manager_put = function(req, res) {
  res.send({ manager: "put" });
  console.log("Put manager: ", req.params);
  console.log(JSON.stringify(req.body));
};

exports.manager_delete = function(req, res) {
  console.log(JSON.stringify(req.body.managerName));
  // retrieve the manager from the database (assuming they are in there)
  var collection = db.get(activeCollection);

  collection.findOneAndDelete({ managerName: req.body.managerName }).then(doc => {
    console.log(doc);

        // return all managers
        collection.find({}).then(docs => {
          //   console.log(docs)
          res.send({
            response: "all managers sent",
            data: docs
          });
        });

  });
};

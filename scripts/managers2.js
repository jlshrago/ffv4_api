"use strict";

const settings = require("./settings");
const db = require("monk")(settings.mongoURL);
var activeCollection = settings.managerCollection;
var auctionCollection = settings.auctionCollection;
var playerCollection = settings.playerCollection;

var async = require("async");

// db.then(() => {
//     console.log("db connected in managers.js as well")
// })

exports.managers_all = function(req, res) {
  var collection = db.get(activeCollection);
  collection.find({}).then(docs => {
    //   console.log(docs)
    res.send({
      response: "all managers sent",
      data: docs
    });
  });
  console.log("Get all managers");
};

exports.managers_addOLD = function(req, res) {
  // add the manager to the database if he/she is not already in there
  var collection = db.get(activeCollection);
  collection.count({ managerName: req.body.managerName }, function(err, count) {
    if (count == 0) {
      // manager is not in managers collection, add them
      collection.insert(req.body);
      console.log("Manager added: ", req.body.managerName);

      // return all managers
      collection.find({}).then(docs => {
        //   console.log(docs)
        res.send({
          response: "all managers sent",
          data: docs
        });
      });
    } else {
      // manager already in db collection, ignore
      console.log("Manager " + req.body.managerName + " already in db");
      res.send({ response: "ignored" });
    }
  });
};

exports.managers_add = function(req, res) {
  // add the manager to the database if he/she is not already in there
  var collection = db.get(activeCollection);
  collection.count({ managerName: req.body.managerName }, function(err, count) {
    if (count == 0) {
      // manager is not in managers collection, add them
      collection.insert(req.body).then(result => {
        console.log("Manager added: ", req.body.managerName);

        // return all managers
        collection.find({}).then(docs => {
          //   console.log(docs)
          res.send({
            response: "all managers sent",
            data: docs
          });
        });
      });
    } else {
      // manager already in db collection, ignore
      console.log("Manager " + req.body.managerName + " already in db");
      res.send({ response: "ignored" });
    }
  });
};

exports.manager_get = function(req, res) {
  console.log(JSON.stringify(req.params.name));
  // retrieve all of the auction events relating to the manager AND all of the players from all of those auction events
  var collection1 = db.get(auctionCollection);
  var collection2 = db.get(playerCollection);

  collection1.find({ manager: req.params.name }).then(docs => {
    // console.log(docs);

    if (docs.length == 0) {
      console.log("Send Nothing!");
      res.send({
        response: "1",
        data: {
          GK: [],
          FB: [],
          CB: [],
          MF: [],
          ST: [],
          SU: []
        }
      });
    }
    // call once done
    var playerCalls = 0;
    var docs2 = [];

    // console.log(docs);

    for (var i = 0; i < docs.length; i++) {
      // go through, getting each one and incrementing playercalls
      collection2.find({ playerName: docs[i].player }).then(docs_2 => {
        docs2.push(docs_2);
        // console.log(docs_2);
        playerCalls++;
        // console.log(playerCalls)
        if (playerCalls === docs.length) {
          // great now combine the two
          var playerList = {
            GK: [],
            FB: [],
            CB: [],
            MF: [],
            ST: [],
            SU: []
          };
          var iterateCalls = 0;

          for (var k = 0; k < docs.length; k++) {
            var playerLine = {};
            playerLine.playerName = docs[k].player;
            playerLine.price = docs[k].price;

            for (var j = 0; j < docs2.length; j++) {
              if (docs2[j][0].playerName == docs[k].player) {
                playerLine.position = docs2[j][0].position;
                playerLine.club = docs2[j][0].club;
                // playerList.push(playerLine);

                switch (playerLine.position) {
                  case "GK":
                    if (playerList["GK"].length < 1) {
                      playerList["GK"].push(playerLine);
                    } else {
                      playerList["SU"].push(playerLine);
                    }
                    break;
                  case "FB":
                    if (playerList["FB"].length < 2) {
                      playerList["FB"].push(playerLine);
                    } else {
                      playerList["SU"].push(playerLine);
                    }
                    break;
                  case "CB":
                    if (playerList["CB"].length < 2) {
                      playerList["CB"].push(playerLine);
                    } else {
                      playerList["SU"].push(playerLine);
                    }
                    break;
                  case "MF":
                    if (playerList["MF"].length < 4) {
                      playerList["MF"].push(playerLine);
                    } else {
                      playerList["SU"].push(playerLine);
                    }
                    break;
                  case "ST":
                    if (playerList["ST"].length < 2) {
                      playerList["ST"].push(playerLine);
                    } else {
                      playerList["SU"].push(playerLine);
                    }
                    break;
                }

                iterateCalls++;

                if (iterateCalls == docs2.length) {
                  // console.log(playerList);
                  console.log("Send Something!");
                  res.send({
                    response: "1",
                    data: playerList
                  });
                }
              }
            }
          }
        }
      });
    }
  });

  /*
  collection1.find({ manager: req.params.name }).then(docs => {
    // console.log(docs);
    var playerList = []
    
    for(var i = 0; i < docs.length; i++) {
      // console.log(docs[i].player)
      var singlePlayer = {};
      singlePlayer.playerName = docs[i].player;
      singlePlayer.cost = docs[i].price;
      // playerList.push(singlePlayer)
      // console.log(playerList)
      
      
    }
    
    for(var i = 0; i < playerList.length; i++) {
      // go through the nascent playerList and add the data from the new calls
      collection2.find({ playerName: playerList[i].playerName }).then(docs2 => {
        // console.log("return2?", docs2)
        playerList.position = docs2[0].position;
        playerList.club = docs2[0].club;
  
        // playerList.push(singlePlayer)
      })

      console.log(playerList)
    }
*/

  // });

  // res.send({
  //   response: "1"
  // });
};

exports.manager_getOLD = function(req, res) {
  console.log(JSON.stringify(req.params.name));
  // retrieve all of the auction events relating to the manager AND all of the players from all of those auction events
  var collection1 = db.get(auctionCollection);
  var collection2 = db.get(playerCollection);

  collection1.find({ manager: req.params.name }).then(docs => {
    var thePlayers = [];
    // get the list of the players the manager has bought

    var totalPlayers = docs.length;
    var iterateCount = 0; // to make sure that the mongo calls return

    if (totalPlayers == 0) {
      res.send({
        response: "0"
      });
    }

    // Iterate over the player list
    for (var i = 0; i < docs.length; i++) {
      var singlePlayer = {};

      console.log(docs[i].player);
      singlePlayer.playerName = docs[i].player;
      singlePlayer.cost = docs[i].price;

      collection2.find({ playerName: docs[i].player }).then(docs2 => {
        singlePlayer.position = docs2[0].position;
        singlePlayer.club = docs2[0].club;

        thePlayers.push(singlePlayer);

        console.log(singlePlayer);

        console.log("Do something!");
      });
    }
    res.send({
      response: "1",
      data: thePlayers
    });
  });
};

exports.manager_put = function(req, res) {
  res.send({ manager: "put" });
  console.log("Put manager: ", req.params);
  console.log(JSON.stringify(req.body));
};

exports.manager_delete = function(req, res) {
  console.log(JSON.stringify(req.body.managerName));
  // retrieve the manager from the database (assuming they are in there)
  var collection = db.get(activeCollection);

  collection
    .findOneAndDelete({ managerName: req.body.managerName })
    .then(doc => {
      console.log(doc);

      // return all managers
      collection.find({}).then(docs => {
        //   console.log(docs)
        res.send({
          response: "all managers sent",
          data: docs
        });
      });
    });
};

"use strict";

const settings = require("./settings");
const db = require("monk")(settings.mongoURL);
const activeCollection = settings.playerCollection;
const secondaryCollection = settings.ratingsCollection;

exports.players_get = function(req, res) {
  var collection = db.get(activeCollection);
  // console.log(req.params);
  // console.log(req.body);

  collection.find({}).then(docs => {
    res.send(docs);
  });
};

exports.players_get_team = function(req, res) {
  // console.log(JSON.stringify(req.params.teamAbbrev))
  // res.send("!")

  // console.log(req.params);
  // console.log(req.body);

  var collection = db.get(activeCollection);

  collection.find({ club: req.params.teamAbbrev }).then(docs => {
    res.send(docs);
  });
};

exports.player_get = function(req, res) {
  console.log("Asking for: ", req.params.playerName);

  var collection = db.get(activeCollection);
  var collection2 = db.get(secondaryCollection);

  var returnItem = {
    details: "",
    ratings: ""
  };

  collection.find({ playerName: req.params.playerName }).then(docs => {
    returnItem["details"] = docs[0];
    // console.log(docs[0])

    collection2.find({ playerName: req.params.playerName }).then(docs2 => {
      returnItem["ratings"] = docs2[0];
      // console.log(docs2[0])
      res.send(returnItem);
    });
  });
};

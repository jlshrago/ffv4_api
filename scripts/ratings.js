"use strict";

const settings = require("./settings");
const db = require("monk")(settings.mongoURL);
const activeCollection = settings.ratingsCollection;

exports.ratings_all = function(req, res) {
  var collection = db.get(activeCollection);

  collection.find({}).then(docs => {
    res.send(docs);
  });
};

exports.rating_add = function(req, res) {
  // this takes rating object, adds it to the rating collection and returns all ratings
  var collection = db.get(activeCollection);

  collection.insert(req.body, function(err, docs) {
    // once inserted, pull all of the auction events and return them
    collection.find({}).then(docs => {
      res.send(docs);
    });
  });
};

exports.rating_add2 = function(req, res) {
  // takes a rating object and updates with upsert to the rating collection.
  var collection = db.get(activeCollection);

  collection.update(
    {
      playerName: req.body.playerName,
      club: req.body.club,
      position: req.body.position
    },
    req.body,
    { upsert: true },
    function(err, res) {
      console.log("DONE ", res);
    }
  );
};

exports.rating_delete = function(req, res) {
  // receives the same auction object of time,player,manager,price and is matched on player, manager, price likely pulled from the object in the dom
  var collection = db.get(activeCollection);

  collection
    .findOneAndDelete({
      playerName: req.body.playerName,
      position: req.body.position,
      club: req.body.club
    })
    .then(doc => {
      // return all, updated auction events
      collection.find({}, function(err, docs) {
        res.send({ docs });
      });
    });
};

exports.rating_delete2 = function(req, res) {
  // receives rating id
  var collection = db.get(activeCollection);

  collection
    .findOneAndDelete({
      _id: req.body.id
    })
    .then(doc => {
      // return all, updated auction events
      collection.find({}, function(err, docs) {
        res.send({ docs });
      });
    });
};

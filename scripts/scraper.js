"use strict";

// called to scrape the FF site and insert/update players into the mongodb

// console.log("Scraper initiated");
const request = require("request");
const cheerio = require("cheerio");
const settings = require("./settings");
const moment = require("moment");

exports.updatePlayers = function(req, res, next) {
  // only now connect to database
  const db = require("monk")(settings.mongoURL);

  // only do stuff once the db has connected

  db.then(() => {
    // check that we are connected to the database
    console.log("db connected");

    console.log("Player updater called");

    // load the page

    request(settings.flURL, function(e, resp, body) {
      // ------------------------------------------------------------
      // ------------------------------------------------------------
      // LOAD FL.COM PAGE INTO CHEERIO TO BE SCRAPE FOR PLAYERS
      // ------------------------------------------------------------
      // ------------------------------------------------------------
      const $ = cheerio.load(body);
      var playerCount = 0;
      var masterJSON = [];

      // ------------------------------------------------------------
      // iterate through the page to pull out players
      // ------------------------------------------------------------
      $("tr").each(function() {
        // load individual player line into cheerio and strip white space
        var playerLine = cheerio.load(this, {
          xml: {
            normalizeWhitespace: true
          }
        });

        // form JSON object for each player
        var playerJSON = {
          playerName: "",
          position: "",
          club: "",
          statPld: "",
          statGls: "",
          statAss: "",
          statCs: "",
          statGa: "",
          statPts: ""
        };

        //   check whether there actually is a player - there are some blanks
        if (
          !playerLine("td[class=highlight] > a")
            .text()
            .replace(/\s/g, "")
        ) {
          // blank line so ignore
        } else {
          // not blank, collect the player details
          playerJSON.playerName = playerLine("td[class=highlight] > a")
            .text()
            .trim();
          playerJSON.position = playerLine('td[class="first no-border"] > div')
            .text()
            .trim();
          playerJSON.club = playerLine('td > a[href*="/Pro/Stats/Club"]')
            .text()
            .trim();
          playerJSON.statPld = playerLine("td:nth-last-child(7)")
            .text()
            .trim();
          playerJSON.statGls = playerLine("td:nth-last-child(6)")
            .text()
            .trim();
          playerJSON.statAss = playerLine("td:nth-last-child(5)")
            .text()
            .trim();
          playerJSON.statCs = playerLine("td:nth-last-child(4)")
            .text()
            .trim();
          playerJSON.statGa = playerLine("td:nth-last-child(3)")
            .text()
            .trim();
          playerJSON.statPts = playerLine("td:nth-last-child(1)")
            .text()
            .trim();
          playerJSON.updateTime = moment().format();

          // add it to the master json for a single insert into the db
          masterJSON.push(playerJSON);
          playerCount += 1;
        }
      });

      // ------------------------------------------------------------
      // masterJSON formed, now to update the player database
      // ------------------------------------------------------------

      // iterate the masterJSON and for each entry check whether the player is in the database, if not add him, if yes then check whether anything has changed, if yes, replace, otherwise leave.

      // ------------------------------------------------------------

      masterJSON.forEach(function(elem) {
        // console.log(elem.playerName)

        // connect to the player collection
        var collection = db.get(settings.playerCollection);

        // check to see if the player/club combination exists in the database
        collection.count(
          { playerName: elem.playerName, club: elem.club },
          function(err, count1) {
            // does this combination existin in the db?
            if (count1 == 0) {
              // player/club combination is not in the db
              // has the player moved club? check to see player/points combination
              collection.count(
                { playerName: elem.playerName, statPts: elem.statPts },
                function(err, count2) {
                  if (count2 == 0) {
                    // player is definitively not in the db, add him
                    collection.insert(elem);
                    console.log('New player "' + elem.playerName + '" added');
                  } else {
                    // player has moved club, update him
                    collection.update(
                      { playerName: elem.playerName, statPts: elem.statPts },
                      elem
                    );
                    console.log(
                      'Player "' +
                        elem.playerName +
                        '" changed club to ' +
                        elem.club
                    );
                  }
                }
              );
            } else {
              // player/club is in the database already, ignore
            }
          }
        );
      });
      res.status(200).send("Player list updated");
    });
  });
};

exports.updateTeams = function(req, res) {
  // only now connect to database
  const db = require("monk")(settings.mongoURL);

  // only do stuff once the db has connected
  db.then(() => {
    console.log("DB connected, update teams called");

    // load page with teams on it
    request(settings.flURL_teams, function(e, resp, body) {
      // ------------------------------------------------------------
      // ------------------------------------------------------------
      // LOAD FL.COM PAGE INTO CHEERIO TO BE SCRAPE FOR TEAMS
      // ------------------------------------------------------------
      // ------------------------------------------------------------
      const $ = cheerio.load(body);

      var teamList = [];
      var teamsAdded = 0;

      $("tr").each(function() {
        var line = cheerio.load(this, {
          xml: {
            normalizeWhitespace: true
          }
        });

        var teamAbbrev = line('td > a[class="club-icon"]')
          .text()
          .trim();

        // var teamName = line('td > a[class="club-icon"]')
        //   .href();

        if (
          teamList.includes(teamAbbrev) ||
          !line('td > a[class="club-icon"]')
            .text()
            .replace(/\s/g, "")
        ) {
          // team already in, leave it
        } else {
          // add it
          teamList.push(teamAbbrev);
        }

        
      });
      
      teamList.forEach(function(elem) {
        var collection = db.get(settings.teamCollection);

        collection.drop().then(function(r) {
          
          collection.insert({
            teamAbbrev: elem,
            teamName: "",
            isActive: true
          }).then(result => {
            console.log(result)
            teamsAdded ++
          })

        })
      })
      
      res.end("Team list updated");
    // res.end("Teams updated: ", teamsAdded);
      // console.log(teamList);
      // console.log(teamList.length);
    });
  });
};

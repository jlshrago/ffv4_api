// holds all relevant settings needed

// the url of the player list
exports.flURL = "http://www.fantasyleague.com/Pro/Stats/playerlist.aspx?dpt=0";
exports.flURL_teams = "http://www.fantasyleague.com/Pro/Stats/PlayerList.aspx";

// uri of the mongo db
exports.mongoURL = "mongodb://jls:aliens6128@ds155730.mlab.com:55730/ff_v4";

// names of collections
exports.playerCollection = "players";
exports.auctionCollection = "auction";
exports.managerCollection = "managers";
exports.ratingsCollection = "ratings";
exports.teamCollection = "teams";

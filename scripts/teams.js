"use strict";

const settings = require("./settings");
const db = require("monk")(settings.mongoURL);
// var activeCollection = settings.managerCollection;
// var auctionCollection = settings.auctionCollection;
var playerCollection = settings.playerCollection;
var teamCollection = settings.teamCollection;

var async = require("async");

// db.then(() => {
//     console.log("db connected in teams.js as well")
// })

exports.teams_all = function(req, res) {
  var collection = db.get(teamCollection);
  collection.find({}).then(docs => {
    //   console.log(docs)
    res.send({
      response: "all teams sent",
      data: docs
    });
  });
  console.log("Get all teams");
};

exports.teams_add = function(req, res) {
  // add the manager to the database if he/she is not already in there
  var collection = db.get(teamCollection);
  collection.count({ teamName: req.body.teamName }, function(err, count) {
    if (count == 0) {
      // manager is not in teams collection, add them
      collection.insert(req.body).then(result => {
        console.log("Manager added: ", req.body.teamName);

        // return all teams
        collection.find({}).then(docs => {
          //   console.log(docs)
          res.send({
            response: "all teams sent",
            data: docs
          });
        });
      });
    } else {
      // manager already in db collection, ignore
      console.log("Manager " + req.body.teamName + " already in db");
      res.send({ response: "ignored" });
    }
  });
};


exports.manager_delete = function(req, res) {
  console.log(JSON.stringify(req.body.teamName));
  // retrieve the manager from the database (assuming they are in there)
  var collection = db.get(activeCollection);

  collection
    .findOneAndDelete({ teamName: req.body.teamName })
    .then(doc => {
      console.log(doc);

      // return all teams
      collection.find({}).then(docs => {
        //   console.log(docs)
        res.send({
          response: "all teams sent",
          data: docs
        });
      });
    });
};

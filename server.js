// server file for API

var express = require("express"),
  app = express(),
  port = process.env.PORT || 5000,
  Task = require("./api/models/model"), //created model loading here
  bodyParser = require("body-parser"),
  settings = require("./scripts/settings"),
  cors = require('cors')

const morgan = require("morgan")
app.use(morgan('tiny'))

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.use(cors()) //  cors enabled!

var routes = require("./api/routes/routes"); //importing routes file
routes(app); //register the route


// the only route on the server - call this to run the scraper for testing
app.get('/', function(req, res) {
  res.status(200).send("Nothing to see here")
});

// handle 404s
app.use(function(req, res) {
  res.status(404).send({ url: req.originalUrl + " not found" });
});

app.listen(port);

console.log("FFv4 API open on: " + port);